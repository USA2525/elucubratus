#!/bin/bash

source "${PKG_BASE}/architect.sh"

export FAKEROOT="fakeroot -i \"${PKG_BASE}/.fakeroot\" -s \"${PKG_BASE}/.fakeroot\""

if [ -z "${PKG_ROOT}" ]; then
    export PKG_ROOT="$(xcrun -sdk ${PKG_SDK} --show-sdk-path)"
fi
if [ -z "${PKG_CCPF}" ]; then
    export PKG_CCPF=$("${PKG_TARG}-gcc" -v 2>&1 | grep -- --prefix | sed -e 's/.*--prefix=\([^ ]*\).*/\1/')
fi

source "${PKG_BASE}/folders.sh"

if [[ ${PKG_NAME} != @(-|:*) ]]; then
    export PKG_MORE=$(PKG_MORE_ "${PKG_NAME}")
    export PKG_DATA=$(PKG_DATA_ "${PKG_NAME}")
    export PKG_WORK=$(PKG_WORK_ "${PKG_NAME}")
    export PKG_DEST=$(PKG_DEST_ "${PKG_NAME}")

    if [ -n "${PKG_PARENT}" ]; then
        export PKG_DATA=$(echo "${PKG_BASE}"/data/"${PKG_PARENT}"?(_))
        export PKG_PARENT_DEST=$(PKG_DEST_ "${PKG_PARENT}")
    else
        export PKG_DATA=$(echo "${PKG_BASE}"/data/"${PKG_NAME}"?(_))
        declare -a PKG_CHILDREN
        for instfile in "${PKG_DATA}"/*.install; do
            CHILD="$(basename ${instfile%.install})"
            PKG_CHILDREN[${#PKG_CHILDREN[@]}]="${CHILD}"
        done
        export PKG_CHILDREN
    fi
    if [[ ! -e ${PKG_DATA} ]]; then
        echo "unknown package: ${PKG_NAME}" 1>&2
        exit 1
    fi
    export PKG_STAT=${PKG_BASE}/stat/${PKG_ARCH}/${PKG_CFTARG}/${PKG_NAME}
    export PKG_RVSN=$(cat "${PKG_STAT}/dest-ver" 2>/dev/null)

    if [[ -e "${PKG_DATA}/_metadata/${PKG_NAME}/zlib" ]]; then
        export PKG_ZLIB=$(cat "${PKG_DATA}/_metadata/${PKG_NAME}/zlib")
    elif [[ -e "${PKG_DATA}/_metadata/zlib" ]]; then
        export PKG_ZLIB=$(cat "${PKG_DATA}/_metadata/zlib")
    else
        export PKG_ZLIB=lzma
    fi

    if [[ -e "${PKG_DATA}/_metadata/${PKG_NAME}/version" ]]; then
        export PKG_VRSN=$(cat "${PKG_DATA}/_metadata/${PKG_NAME}/version")
    elif [[ -e "${PKG_DATA}/_metadata/version" ]]; then
        export PKG_VRSN=$(cat "${PKG_DATA}/_metadata/version")
    fi

    if [[ -e "${PKG_DATA}/_metadata/${PKG_NAME}/priority" ]]; then
        export PKG_PRIO=$(cat "${PKG_DATA}/_metadata/${PKG_NAME}/priority")
    elif [[ -e "${PKG_DATA}/_metadata/priority" ]]; then
        export PKG_PRIO=$(cat "${PKG_DATA}/_metadata/priority")
    fi

    declare -a PKG_DEPS
    for dep in "${PKG_DATA}/_metadata/${PKG_NAME}"/*.dep; do
        PKG_DEPS[${#PKG_DEPS[@]}]=$(basename "${dep}" .dep)
    done

    for dep in "${PKG_DATA}"/_metadata/*.dep; do
        PKG_DEPS[${#PKG_DEPS[@]}]=$(basename "${dep}" .dep)
    done

    for file in "${PKG_DATA}"/_metadata/* "${PKG_DATA}"/_metadata/${PKG_NAME}/*; do
        key="$(basename ${file})"
        if [[ ${key} == *."${PKG_ARCH}" ]]; then
            key="${key%.${PKG_ARCH}}_ARCH"
        elif [[ ${key} == *.* || ${key} == *-* ]]; then
            continue
        fi
        variable="PKG_FILE_$(tr '[:lower:]+' '[:upper:]_' <<< ${key})";
        declare "$variable"="${file}"
        export $variable
    done 
    if [[ -n "${PKG_PARENT}" ]]; then
        export PKG_FILE_DESCRIPTION="${PKG_DATA}/_metadata/${PKG_NAME}/description"
    fi
fi
