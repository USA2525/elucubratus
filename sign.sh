#!/bin/bash
set -e
shopt -s extglob nullglob

if [[ "${REPO}" == "test" ]]; then
    BASEURL="test.apt.bingner.com"
else
    BASEURL="apt.bingner.com"
fi


PKG_BASE=$(dirname "$(realpath "$0")")
cd "${PKG_BASE}"
PKG_RVSN=1

PKG_REPO="${PKG_BASE}/apt/"

make_releasefile () {
    COMPONENTS="${@%/}"
    {
        cat <<EOF
Origin: Bingner/Elucubratus
Label: Bingner/Elucubratus
Suite: stable
Version: 1.0r${PKG_RVSN}
Codename: ios
Architectures: ${RELEASE_ARCHS[@]}
Components: ${COMPONENTS}
Description: Distribution of Unix Software for iPhoneOS
Support: https://cydia.saurik.com/api/support/*
Depiction: https://${BASEURL}/info/*
MD5Sum:
EOF

        find * -type f | grep -v Release | while read -r line; do
            echo " $(md5sum "${line}" | cut -d ' ' -f 1) $(/usr/bin/stat -f %z "${line}" | cut -d $'\t' -f 1) ${line}"
        done

    } >"Release"

    rm -f Release.gpg InRelease
    if [[ " ${RELEASE_ARCHS[@]} " =~ " iphoneos-arm " ]]; then
        gpg -abs -o Release.gpg Release
        gpg -as --clear-sign -o InRelease Release
    fi
}

cd "${PKG_REPO}/dists/ios/"

for PKG_CFTARG in */; do
    echo Generating ${CFTARG%/} Release files
    pushd ${PKG_CFTARG} > /dev/null
    COMPONENTS=*/
    RELEASE_ARCHS=(*/binary-*/)
    RELEASE_ARCHS=(${RELEASE_ARCHS[@]%/})
    RELEASE_ARCHS=(${RELEASE_ARCHS[@]##*/binary-})
    make_releasefile ${COMPONENTS[@]}
    for COMPONENT in ${COMPONENTS[@]}; do
        pushd ${COMPONENT} > /dev/null
        for BINARY_ARCH in binary*/; do
            RELEASE_ARCHS=(${BINARY_ARCH%/})
            RELEASE_ARCHS=(${RELEASE_ARCHS[@]##binary-})
            pushd ${BINARY_ARCH} > /dev/null
            make_releasefile ${COMPONENT}
            popd > /dev/null
        done
        popd > /dev/null
    done
    popd > /dev/null
done
diff -x Release.gpg -x InRelease -x Release -x Packages.xz -x Packages.bz2 -x info -ur "${PKG_BASE}/apt-old/" "${PKG_REPO}" 
