pkg: mkdir -p /usr/lib/llvm-10/bin /usr/bin
for bin in tapi tapi-frontend tapi-binary-reader tapi-import tapi-run; do
    cp -a "$(PKG_DEST_ _llvm10)"/usr/lib/llvm-10/bin/${bin} "${PKG_DEST}"/usr/lib/llvm-10/bin/
    pkg: ln -s ../lib/llvm-10/bin/${bin} /usr/bin/${bin}-10
done
