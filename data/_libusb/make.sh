pkg:setup
# -DMAC_OS_X_VERSION_MIN_REQUIRED is just to make the headers do what we want
CFLAGS="${CFLAGS} -D__OPEN_SOURCE__ -DMAC_OS_X_VERSION_MIN_REQUIRED=101100" pkg:configure
pkg:make V=1
pkg:install
subpkg:stage
