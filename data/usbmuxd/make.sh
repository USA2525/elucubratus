pkg:setup
autoreconf -f -i
pkg:configure --without-cython CC="${PKG_TARG}-gcc" CXX="${PKG_TARG}-g++" ac_cv_func_malloc_0_nonnull=yes ac_cv_func_realloc_0_nonnull=yes
pkg:make V=1
pkg:install
pkg: mkdir -p /Library/LaunchDaemons
pkg: cp %/usbmuxd.plist /Library/LaunchDaemons/
pkg: strip /usr/sbin/usbmuxd
