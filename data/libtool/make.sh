pkg:setup
pkg:configure
pkg:make
pkg:install
GREP=$(grep '^GREP = ' ${PKG_WORK}/*/Makefile)
SED=$(grep '^SED = ' ${PKG_WORK}/*/Makefile)
LD=$(grep '^LD = ' ${PKG_WORK}/*/Makefile)
NM=$(grep '^NM = ' ${PKG_WORK}/*/Makefile)
echo sed -i'' \
    -e "s|${GREP##GREP = }|/bin/grep|" \
    -e "s|${SED##SED = }|/bin/sed|" \
    "${PKG_DEST}/$(cat ${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/prefix)"/bin/{libtool,libtoolize}
sed -i'' \
    -e "s|${GREP##GREP = }|/bin/grep|" \
    -e "s|${SED##SED = }|/bin/sed|" \
    -e "s|${LD##LD = }|/usr/bin/ld|" \
    -e "s|${NM##NM = }|/usr/bin/nm|" \
    -e "s|${PKG_TARG}-||" \
    "${PKG_DEST}/$(cat ${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/prefix)"/bin/{libtool,libtoolize}
