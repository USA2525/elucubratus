LLVM_VERSION=$(sed -rne "s,^([0-9]+).*,\1,p" < "${PKG_DATA}"/_metadata/version)
LLVM_VERSION_FULL=$(sed -rne "s,^([0-9.]+)(~|-)(.*),\1,p" < "${PKG_DATA}"/_metadata/version)
if [ "$1" != "compile" ]; then
pkg:extract
mv tapi* libtapi
cd libtapi
patch -p1 < "${PKG_DATA}/libtapi-arm64e-diff"
cd ..
mv libtapi llvm*/llvm/projects/
cd */
pkg:patch
else
    cd */
fi
if [ "$1" != "compile" ]; then
mv llvm/projects/libtapi ../
rm -rf native
mkdir native
cd native
MACOSX_DEPLOYMENT_TARGET="$(sw_vers -productVersion | sed -e 's/\.[0-9]*$//')" CC="$(xcrun -f clang)" CXX="$(xcrun -f clang++)" cmake -j16 -DCMAKE_INSTALL_NAME_DIR="/usr/local/lib" -DCMAKE_INSTALL_LIBDIR="local/lib" -DLLVM_BUILD_LLVM_DYLIB=ON -DLLVM_ENABLE_PROJECTS="clang;lldb" -DCMAKE_BUILD_TYPE=RELEASE -DLLVM_INCLUDE_TESTS=OFF ../llvm
make -j16 llvm-tblgen clang-tblgen lldb-tblgen
cd ..
mv ../libtapi llvm/projects/
mkdir build
fi
pwd
cd build
unset MIGCC
export MIGCC

unset MACOSX_DEPLOYMENT_TARGET
TARGET_TRIPLE=""
case "${PKG_ARCH}" in
    "iphoneos-"*)
        TARGET_TRIPLE="arm64-apple-ios7.0"
        ;;
    "appletvos-"*)
        TARGET_TRIPLE="arm64-apple-tvos9.0"
        ;;
    "watchos-"*)
        TARGET_TRIPLE="arm64-apple-watchos2.0"
        ;;
    *)
        echo "Unsupported arch ${PKG_ARCH} - edit make.sh"
        exit -1
        ;;
esac

if [ "$1" != "compile" ]; then
cat >iphoneos_toolchain.cmake <<EOF
set(CMAKE_SYSTEM_NAME Darwin)
# Tell CMake we're cross-compiling
set(CMAKE_CROSSCOMPILING true)
#include(CMakeForceCompiler)
# Prefix detection only works with compiler id "GNU"
# CMake will look for prefixed g++, cpp, ld, etc. automatically
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(triple ${PKG_TARG})
set(CMAKE_FIND_ROOT_PATH $(echo ${PKG_PATH} | sed -e s/:/' '/g))
set(CMAKE_LIBRARY_PATH $(echo ${LIBRARY_PATH} | sed -e s/:/' '/g))
set(CMAKE_INCLUDE_PATH $(echo ${INCLUDE_PATH} | sed -e s/:/' '/g))
set(CMAKE_C_COMPILER ${PKG_TARG}-gcc)
set(CMAKE_CXX_COMPILER ${PKG_TARG}-g++)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
EOF
#set(CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG 1)

#cmake -j16 -DCMAKE_TOOLCHAIN_FILE=iphoneos_toolchain.cmake -DCMAKE_INSTALL_NAME_DIR="/usr/local/lib" -DCMAKE_INSTALL_LIBDIR="local/lib" -DCMAKE_INSTALL_RPATH="/usr/" -DCMAKE_OSX_SYSROOT="${PKG_ROOT}" -DCMAKE_INSTALL_PREFIX="/usr/" -DLLVM_INCLUDE_TESTS=OFF -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_SYSTEM_NAME=Darwin -DLLVM_TABLEGEN="$(pwd)/../native/bin/llvm-tblgen" -DCLANG_TABLEGEN="$(pwd)/../native/bin/clang-tblgen" -DLLDB_TABLEGEN="${PWD}/../native/bin/lldb-tblgen" -DLLVM_BUILD_LLVM_DYLIB=ON -DLLVM_LINK_LLVM_DYLIB=ON -DLIBCXX_OVERRIDE_DARWIN_INSTALL=ON -DLLVM_DEFAULT_TARGET_TRIPLE="${TARGET_TRIPLE}" -DLLVM_ENABLE_PROJECTS="clang;libcxx;libcxxabi;lldb" -DLLDB_FRAMEWORK_INSTALL_DIR="/Library/Frameworks" ../llvm
cmake -j16 -DCMAKE_TOOLCHAIN_FILE=iphoneos_toolchain.cmake -DCMAKE_OSX_SYSROOT="${PKG_ROOT}" -DCLANG_VERSION="${LLVM_VERSION_FULL}" -DCMAKE_INSTALL_NAME_DIR="/usr/lib/llvm-${LLVM_VERSION}/lib" -DCMAKE_INSTALL_RPATH="/usr/lib/llvm-${LLVM_VERSION}" -DCMAKE_INSTALL_PREFIX="/usr/lib/llvm-${LLVM_VERSION}" -DLLVM_INCLUDE_TESTS=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_SYSTEM_NAME=Darwin -DLLVM_TABLEGEN="${PWD}/../native/bin/llvm-tblgen" -DCLANG_TABLEGEN="${PWD}/../native/bin/clang-tblgen" -DLLDB_TABLEGEN="${PWD}/../native/bin/lldb-tblgen" -DLLVM_BUILD_LLVM_DYLIB=ON -DLLVM_LINK_LLVM_DYLIB=ON -DLIBCXX_OVERRIDE_DARWIN_INSTALL=ON -DLLVM_DEFAULT_TARGET_TRIPLE="${TARGET_TRIPLE}" -DCLANG_LINK_CLANG_DYLIB=ON -DLLVM_ENABLE_PROJECTS="clang;compiler-rt;libcxx;libcxxabi;lldb;tapi" -DCLANG_TABLEGEN_EXE="${PWD}/../native/bin/clang-tblgen" -DLLVM_VERSION_SUFFIX="" ../llvm
# Need to fix CMakeLists.txt but this makes it build the clang deps before tapi
make -j16 clang
fi
make -j16
pkg:install
pkg: ln -s lib/llvm-10/include /usr/include
pkg: ldid -S"${PKG_DATA}/debugserver.xml" /usr/lib/llvm-10/bin/debugserver
#pkg: mkdir -p /usr/local/lib
#mv ${PKG_DEST}/usr/lib/*.dylib ${PKG_DEST}/usr/local/lib
#pkg: find /usr/lib -name '*.a' -exec rm {} +
#pkg: mkdir -p /usr/share/SDKs
cd ..

# clang-10
pkg: mkdir -p /usr/bin
pkg: ln -s ../lib/llvm-10/bin/clang-10 /usr/bin/
pkg: ln -s ../lib/llvm-10/bin/clang-10 /usr/bin/clang++-10
pkg: ln -s ../lib/llvm-10/bin/clang-10 /usr/bin/clang-cpp-10

subpkg:stage
