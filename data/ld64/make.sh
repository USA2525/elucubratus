pkg:setup
pkg:make VERSION="$(pkg: cat %/_metadata/version)" LDFLAGS="-L$(PKG_DEST_ _llvm10)/usr/lib/llvm-10/lib"
pkg:install
pkg: mkdir -p /usr/libexec
pkg: mv /usr/bin/ld64 /usr/libexec/ld64
pkg: ln -s ld64 /usr/bin/ld
pkg: "${PKG_TARG}-gcc" %/wrapper.c -o /usr/bin/ld64
if [[ -f "${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/entitlement.xml" ]]; then
  pkg: mkdir -p /usr/share/entitlements
  cp "${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/entitlement.xml" "${PKG_DEST}/usr/share/entitlements/ld64.xml"
fi
