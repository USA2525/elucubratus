pkg:extract
pkg: cp %/certdata.txt .
$(PKG_WORK_ curl)/*/lib/mk-ca-bundle.pl  -n
pkg: mkdir -p /etc/ssl/certs /etc/profile.d /usr/lib/ssl
pkg: cp ca-bundle.crt /etc/ssl/certs/cacert.pem
pkg: cp %/cacerts.bootstrap.sh /etc/profile.d/
pkg: ln -s certs/cacert.pem /usr/lib/ssl/cert.pem
