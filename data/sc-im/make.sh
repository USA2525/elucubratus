pkg:setup
cd src
pkg:make -j16 prefix="${PKG_TAPF}" PKGCONFIG="${PKG_BASE}/util/pkg-config.sh"
pkg:install prefix="${PKG_TAPF}" PKGCONFIG="${PKG_BASE}/util/pkg-config.sh"
