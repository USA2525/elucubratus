pkg:setup
rm -f include/mach/i386/_structs.h
VERS="$(pkg: cat %/_metadata/version)"
pkg:make RC_ProjectSourceVersion="${VERS}"
#pkg:make DSTROOT="${PKG_DEST}" SDKROOT="${PKG_ROOT}" RC_ProjectSourceVersion="${VERS}"
#make install CC="${PKG_TARG}-gcc" CXX="${PKG_TARG}-g++" AR="${PKG_TARG}-ar" DSTROOT="${PKG_DEST}" SDKROOT="${PKG_ROOT}" RC_ProjectSourceVersion="${VERS}"
make install CC="${PKG_TARG}-gcc" CXX="${PKG_TARG}-g++" AR="${PKG_TARG}-ar" DSTROOT="${PKG_DEST}" RC_ProjectSourceVersion="${VERS}"
pkg: rm -f /usr/lib/system/libmacho.dylib /usr/bin/{nm,otool,size}
pkg: mv -f /usr/bin/ar{,-classic}
pkg: ln -s llvm-ar /usr/bin/ar
pkg: mv -f /usr/bin/ranlib{,-classic}
pkg: ln -s llvm-ranlib /usr/bin/ranlib
pkg: mv -f /usr/bin/nm{-classic,}
pkg: mv -f /usr/bin/otool{-classic,}
pkg: mv -f /usr/bin/size{-classic,}
