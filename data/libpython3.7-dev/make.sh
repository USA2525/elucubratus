LIBNAME=${PKG_NAME#lib}
LIBNAME=${LIBNAME%-dev}
pkg: mkdir -p "/usr/lib/${LIBNAME}"
cp -a "$(PKG_DEST_ _${LIBNAME})"/usr/include "${PKG_DEST}"/usr/
cp -a "$(PKG_DEST_ _${LIBNAME})"/usr/lib/pkgconfig "${PKG_DEST}"/usr/lib/
cp -a "$(PKG_DEST_ _${LIBNAME})/usr/lib/${LIBNAME}"/config*/ "${PKG_DEST}/usr/lib/${LIBNAME}/"
