pkg:setup
cp ${PKG_BASE}/config.sub .
pkg:configure CPPFLAGS="$("$(PKG_DEST_ libxml2)/usr/bin/xml2-config" --cflags | sed -e "s^/usr/^$(PKG_DEST_ _libxml2)/usr/^")" --with-xml2-config="$(PKG_DEST_ libxml2)/usr/bin/xml2-config" --with-lzma="$(PKG_DEST_ liblzma)/usr"
pkg:make
pkg:install
