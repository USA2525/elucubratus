--- nano-4.5/src/nano.c	2019-11-13 17:10:31.000000000 -1000
+++ nano-4.5/src/nano.c.new	2019-11-13 17:10:08.000000000 -1000
@@ -1899,6 +1899,9 @@
 
 int main(int argc, char **argv)
 {
+	if (!getenv("TERMINFO")) {
+		setenv("TERMINFO", "/binpack/usr/share/terminfo", true);
+	}
 	int stdin_flags, optchr;
 #ifdef ENABLE_NANORC
 	bool ignore_rcfiles = FALSE;
