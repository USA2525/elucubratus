#!/bin/bash
set -e
shopt -s extglob nullglob

if [[ "${REPO}" == "live" ]]; then
    BASEURL="https://apt.bingner.com"
elif [[ "${REPO}" == "test" ]]; then
    BASEURL="https://test.apt.bingner.com"
else
    echo "Please define REPO as \"live\" or \"test\""
    exit 1
fi

PKG_BASE=$(dirname "$(realpath "$0")")
cd "${PKG_BASE}"
PKG_RVSN=1

PKG_REPO="${PKG_BASE}/apt/"

rm -rf "${PKG_BASE}/apt-old"
cp -a "${PKG_REPO}" "${PKG_BASE}/apt-old"
rm -rf "${PKG_REPO}/"{debs,dists,info}/
mkdir -p "${PKG_REPO}/debs/"
rm -f "${PKG_BASE}/extra-overrides.txt"

for json in changelogs/*.json; do
    PKGID="$(basename "${json}" .json)"
    if ! [ -d "apt/info/${PKGID}" ]; then
        mkdir -p "apt/info/${PKGID}"
    fi
    perl makeinfo.pl "${json}" > apt/info/"${PKGID}"/index.html
    echo "${PKGID} Depiction ${BASEURL}/info/${PKGID}/" >> extra-overrides.txt
done

for PKG_ARCH in "${PKG_BASE}/arch"/*/; do
    if [[ -e "${PKG_BASE}/$environ.sh" ]]; then
        source "${PKG_BASE}/$environ.sh"
    fi
    for PKG_CFTARG in "${PKG_ARCH}"/*/; do
        if [[ -e "${PKG_BASE}/$environ.sh" ]]; then
            source "${PKG_CFTARG}/$environ.sh"
        fi
        PKG_ARCH=$(basename "${PKG_ARCH}")
        PKG_DIST=ios

        PKG_CFTARG=$(basename "${PKG_CFTARG}")
        echo "scanning ${PKG_ARCH}/${PKG_CFTARG}"

        PKG_DCBF=${PKG_REPO}/dists/${PKG_DIST}/${PKG_CFTARG}/main/binary-${PKG_ARCH}
        mkdir -p "${PKG_DCBF}"
        PKG_PKGS=${PKG_DCBF}/Packages

        rm -rf "${PKG_BASE}/link"
        mkdir -p "${PKG_BASE}/link/${PKG_CFTARG}"

        for package in "${PKG_BASE}/data"/!(*_) "${PKG_BASE}/data"/_*/*.install; do

            if [[ $package == *.install ]]; then
                PKG_NAME=$(basename "${package}" .install)
                PKG_PARENT=$(basename "$(dirname ${package})")
                package="$(dirname ${package})"
            else
                PKG_PARENT=''
                PKG_NAME=$(basename "${package}")
            fi
            # XXX: add to above filter
            if [[ ${PKG_NAME} == _* || ! -f "${package}/_metadata/in.${PKG_CFTARG}" ]]; then
                continue
            fi

            PKG_STAT="${PKG_BASE}/stat/${PKG_ARCH}/${PKG_CFTARG}/${PKG_NAME}"
            if [[ -z "${PKG_PARENT}" ]]; then
                PKG_DATA="${PKG_BASE}/data/${PKG_NAME}"
                PKG_PRIO=$(cat "${PKG_DATA}/_metadata/priority")
                if ! [ -f "apt/info/${PKG_NAME}/index.html" ]; then
                    mkdir -p "apt/info/${PKG_NAME}"
                    perl makeinfo.pl "${PKG_NAME}" "${PKG_BASE}/data/${PKG_NAME}/_metadata" > apt/info/"${PKG_NAME}"/index.html
                    echo "${PKG_NAME} Depiction ${BASEURL}/info/${PKG_NAME}/" >> extra-overrides.txt
                fi
            else
                PKG_DATA="${PKG_BASE}/data/${PKG_PARENT}"
                PKG_PRIO=$(cat "${PKG_DATA}/_metadata/${PKG_NAME}/priority" 2>/dev/null || cat "${PKG_DATA}/_metadata/priority")
                if ! [ -f "apt/info/${PKG_NAME}/index.html" ]; then
                    mkdir -p "apt/info/${PKG_NAME}"
                    perl makeinfo.pl "${PKG_NAME}" "${PKG_BASE}/data/${PKG_PARENT}/_metadata/${PKG_NAME}" "${PKG_BASE}/data/${PKG_PARENT}/_metadata" > apt/info/"${PKG_NAME}"/index.html
                    echo "${PKG_NAME} Depiction ${BASEURL}/info/${PKG_NAME}/" >> extra-overrides.txt
                fi
            fi

            if [[ -e ${PKG_STAT}/fail ]]; then
                continue
            fi

            echo "${PKG_NAME}" "${PKG_PRIO#+}" "$(cat "${PKG_DATA}/_metadata/section")"

            PKG_FILE=${PKG_STAT}/dest-ver
            if [[ -e ${PKG_FILE} ]]; then
                PKG_REAL=${PKG_STAT}/real-ver
                if [[ -e ${PKG_REAL} ]]; then
                    PKG_RVER=$(cat "${PKG_REAL}")
                else
                    if [[ -z "${PKG_PARENT}" ]]; then
                        PKG_RVER=$(cat "${PKG_STAT}/data-ver")-$(cat "${PKG_FILE}")
                    else
                        PKG_RVER=$(cat "${PKG_BASE}/stat/${PKG_ARCH}/${PKG_CFTARG}/${PKG_PARENT}/data-ver")-$(cat "${PKG_FILE}")
                    fi
                fi

                PKG_FILE=${PKG_BASE}/debs/${PKG_CFTARG}/${PKG_NAME}_$(sed -e s/:/_/g <<<"${PKG_RVER}")_${PKG_ARCH}.deb
                if [[ -e ${PKG_FILE} && ! -e "${PKG_STAT}/exclude" ]]; then
                    ln -s "${PKG_FILE}" "${PKG_BASE}/link/${PKG_CFTARG}/"
                fi
            fi
        done >"${PKG_BASE}/overrides.txt"

        for deb in "${PKG_BASE}/xtra/${PKG_ARCH}/${PKG_CFTARG}"/*.deb; do
            ln -s "$(readlink -f "${deb}")" "${PKG_BASE}/link/${PKG_CFTARG}/"
        done

        for deb in "${PKG_BASE}/xtra/all/${PKG_CFTARG}"/*.deb; do
            ln -s "$(readlink -f "${deb}")" "${PKG_BASE}/link/${PKG_CFTARG}/"
        done

        dpkg-scanpackages -h md5,sha1,sha256 -e "${PKG_BASE}/extra-overrides.txt" -m link "${PKG_BASE}/overrides.txt" | sed -e 's/: link\//: debs\//' | while IFS= read -r line; do
            if [[ ${line} == '' ]]; then
                PKG_TAGS=$(cat "${PKG_BASE}/tags/${PKG_NAME}" 2>/dev/null || true)
                if [[ -z ${PKG_TAGS} ]]; then
                    PKG_TAGS=$(cat "${PKG_BASE}/data/${PKG_NAME}/_metadata/tags" 2>/dev/null || true)
                fi
                PKG_ROLE="${PKG_BASE}/data/${PKG_NAME}/_metadata/role"
                if [[ -n ${PKG_TAGS} || -e ${PKG_ROLE} ]]; then
                    echo -n "Tag: "
                    if [[ -n ${PKG_TAGS} ]]; then
                        echo -n "${PKG_TAGS}"
                    fi
                    if [[ -n ${PKG_TAGS} && -e ${PKG_ROLE} ]]; then
                        echo -n ", "
                    fi
                    if [[ -e ${PKG_ROLE} ]]; then
                        echo -n "role::$(cat "${PKG_ROLE}")"
                    fi
                    echo
                fi
            elif [[ ${line} == Package:* ]]; then
                PKG_NAME=${line#Package: }
            fi

            echo "${line}"
        done >"${PKG_PKGS}"
		    cp -a "${PKG_BASE}/link"/* "${PKG_REPO}/debs/"
#rm -f "${PKG_BASE}/overrides.txt"
        bzip2 -c "${PKG_PKGS}" >"${PKG_PKGS}.bz2"
        xz -c "${PKG_PKGS}" >"${PKG_PKGS}.xz"
    done
done

"${PKG_BASE}/sign.sh"
