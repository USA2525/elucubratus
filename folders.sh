#!/bin/bash

function PKG_DATA_() {
    if [[ -n "${PKG_PARENT}" && "$1" != "${PKG_PARENT}" ]]; then
        echo "${PKG_BASE}/data/${PKG_PARENT}"
    else
        echo "${PKG_BASE}/data/$1"
    fi
}

export -f PKG_DATA_

function PKG_WORK_() {
    if [[ -n "${PKG_PARENT}" && "$1" != "${PKG_PARENT}" ]]; then
        echo "${PKG_BASE}/work/${PKG_ARCH}/${PKG_CFTARG}/${PKG_PARENT}"
    else
        echo "${PKG_BASE}/work/${PKG_ARCH}/${PKG_CFTARG}/$1"
    fi
}

export -f PKG_WORK_

function PKG_MORE_() {
    if [[ -n "${PKG_PARENT}" && "$1" != "${PKG_PARENT}" ]]; then
        echo "${PKG_BASE}/more/${PKG_ARCH}/${PKG_CFTARG}/$PKG_PARENT"
    else
        echo "${PKG_BASE}/more/${PKG_ARCH}/${PKG_CFTARG}/$1"
    fi
}

function PKG_DEST_() {
    if [[ -n "${PKG_PARENT}" && "$1" != "${PKG_PARENT}" ]]; then
        echo "${PKG_BASE}/subdest/${PKG_ARCH}/${PKG_CFTARG}/${PKG_PARENT}/$1"
    else
        echo "${PKG_BASE}/dest/${PKG_ARCH}/${PKG_CFTARG}/$1"
    fi
}

export -f PKG_DEST_

function pkg_ {
    case "${1:0:1}" in
        (/) echo "${PKG_DEST}$1";;
        (@) echo "${PKG_MORE}${1:1}";;
        (%) echo "${PKG_DATA}${1:1}";;
        (*) echo -"$1" | sed -e 's/^.//';;
    esac
}

export -f pkg_

function pkg: {
    declare -a argv
    declare argc=$#

    for ((i=0; $i != $argc; ++i)); do
        argv[$i]="$(pkg_ "$1")"
        shift
    done

    ${FAKEROOT} "${argv[@]}"
}

export -f pkg:
